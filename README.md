# TITLE TO BE DETERMINED


### [Documentation](https://delskayn.gitlab.io/rust-gui-test)


# TODO

This is a list of required features which the library needs to support before showing of its capabilities.

 - [x] Basic layout system
 - [ ] Input events
    * [ ] Pointer events
    * [ ] Keyboard events
 - [ ] Mutating state solution.
 - [ ] Text.
    - [ ] Font loading
    - [ ] Shaping Glyphs
    - [ ] Rendering glyphs (Possibly done via webrenderer)
 - [ ] Dynamic layout
    * [ ] List view
    * [ ] Grid
 - [ ] Styling
 - [ ] Scroll
 - [ ] Examples
     - [ ] Calculator example
     - [ ] Spreadsheet example
