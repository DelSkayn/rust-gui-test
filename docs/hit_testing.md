Hit testing
===========

I currently see two approches to  hit testting.

1. The old way where each element implements a hit testing by passing down events to their childeren
2. Use the webrenderer hit test functionality.

Each of these options has thier own advantages and disadvantages.

Old version
-----------

### Pros

- Allows custom dehavior in hit testing different then using webrenderer/
- Hit testing is not coupled with webrenderer making a possible transition to a different render backend easier.

### Cons

- Requires more work when implementing a new element
- Might be difficult to do well


Webrenderer
-----------

### Pros

- Tested accurate implementation
- Already implemented

### Cons

- Breaks with traversal based implementation currently used
- Might require large changes to current api.
- Tying implementation closer to webrenderer


The Choise
----------

I think going with webrenderer for the backend currently is the best choise.
It is already implemented and works
On problem might be tying the hit test results back to the proper element but i have no doubt that I am able to figure something out. 
