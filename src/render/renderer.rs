use crate::ty::Size;
use gleam::gl;
use glutin::{
    dpi::{LogicalSize, PhysicalSize},
    EventsLoop, EventsLoopProxy, PossiblyCurrent, RawContext,
};
use webrender::{
    api::{
        ColorF, DebugFlags, DeviceIntSize, DocumentId, Epoch, PipelineId, RenderApi, RenderNotifier,
    },
    euclid::{TypedRect, TypedSize2D},
    Renderer as WrRenderer, RendererOptions,
};

struct Notifier {
    events_proxy: EventsLoopProxy,
}

impl Notifier {
    fn new(events_proxy: EventsLoopProxy) -> Notifier {
        Notifier { events_proxy }
    }
}

impl RenderNotifier for Notifier {
    fn clone(&self) -> Box<dyn RenderNotifier> {
        Box::new(Notifier {
            events_proxy: self.events_proxy.clone(),
        })
    }

    fn wake_up(&self) {
        #[cfg(not(target_os = "android"))]
        let _ = self.events_proxy.wakeup();
    }

    fn new_frame_ready(
        &self,
        _: DocumentId,
        _scrolled: bool,
        _composite_needed: bool,
        _render_time: Option<u64>,
    ) {
        self.wake_up();
    }
}

pub struct Renderer {
    pub(crate) renderer: Option<WrRenderer>,
    pub(crate) api: RenderApi,
    pub(crate) epoch: Epoch,
    pub(crate) pipeline_id: PipelineId,
    pub(crate) document_id: DocumentId,
    pub(crate) size: Size,
    pub(crate) dpi: f64,
    pub(crate) context: RawContext<PossiblyCurrent>,
}

impl Renderer {
    pub fn new(
        events_loop: &EventsLoop,
        context: RawContext<PossiblyCurrent>,
        dpi: f64,
        size: Size,
    ) -> Self {
        let gl = match context.get_api() {
            glutin::Api::OpenGl => unsafe {
                gl::GlFns::load_with(|symbol| context.get_proc_address(symbol) as *const _)
            },
            glutin::Api::OpenGlEs => unsafe {
                gl::GlesFns::load_with(|symbol| context.get_proc_address(symbol) as *const _)
            },
            glutin::Api::WebGl => unimplemented!(),
        };

        info!("OpenGL version {}", gl.get_string(gl::VERSION));
        info!("device pixel ratio: {}", dpi);

        let opts = RendererOptions {
            device_pixel_ratio: dpi as f32,
            clear_color: Some(ColorF::new(1.0, 1.0, 1.0, 1.0)),
            //scatter_gpu_cache_updates: false,
            debug_flags: DebugFlags::ECHO_DRIVER_MESSAGES
                | DebugFlags::TEXTURE_CACHE_DBG
                | DebugFlags::PRIMITIVE_DBG,
            ..Default::default()
        };

        let notifier = Box::new(Notifier::new(events_loop.create_proxy()));
        let (renderer, sender) = WrRenderer::new(gl.clone(), notifier, opts, None).unwrap();
        let api = sender.create_api();
        let p_size: (u32, u32) = LogicalSize::from(size).to_physical(dpi).into();
        let document_id = api.add_document(DeviceIntSize::new(p_size.0 as i32, p_size.1 as i32), 0);

        let epoch = Epoch(0);
        let pipeline_id = PipelineId(0, 0);
        Renderer {
            renderer: Some(renderer),
            api,
            epoch,
            pipeline_id,
            document_id,
            size,
            dpi,
            context,
        }
    }

    pub fn resize(&mut self, size: Size) {
        self.size = size;
        let l_size: LogicalSize = size.into();
        let p_size: PhysicalSize = l_size.to_physical(self.dpi);
        let i_size: (u32, u32) = p_size.into();
        let t_size = TypedSize2D::new(i_size.0 as i32, i_size.1 as i32);
        self.api.set_window_parameters(
            self.document_id,
            t_size,
            TypedRect::from_size(t_size),
            self.dpi as f32,
        );
        self.context.resize(p_size);
    }

    pub fn frame(&mut self) {
        let renderer = self.renderer.as_mut().unwrap();
        let p_size: (u32, u32) = LogicalSize::from(self.size).to_physical(self.dpi).into();
        renderer.update();
        renderer
            .render(DeviceIntSize::new(p_size.0 as i32, p_size.1 as i32))
            .unwrap();
        let _ = renderer.flush_pipeline_info();
        self.update_epoch();
        self.context.swap_buffers().unwrap();
    }

    fn update_epoch(&mut self) {
        let Epoch(e) = self.epoch;
        let new_epoch = Epoch(e.wrapping_add(1));
        self.epoch = new_epoch;
        if self.epoch == Epoch::invalid() {
            self.update_epoch();
        }
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        self.renderer.take().unwrap().deinit();
    }
}
