//! Functionality related to the render backend, currently webrender

use glutin::{
    dpi::LogicalPosition, ContextBuilder, ControlFlow, ElementState, Event as GlutinEvent,
    EventsLoop, GlRequest, WindowBuilder, WindowEvent,
};
mod renderer;
pub use self::renderer::Renderer;
mod builder;
use crate::{
    event::{Event, PointerEvent},
    ty::{Constraint, Position},
    Ctx, Element,
};
pub use builder::RenderBuilder;

pub fn render<S, E: Element<S>>(mut state: S, mut element: E) {
    let mut ctx = Ctx::default();
    let mut events_loop = EventsLoop::new();
    let window = WindowBuilder::new()
        .with_resizable(true)
        .with_title("gui_test");

    let (context, window) = unsafe {
        ContextBuilder::new()
            .with_gl(GlRequest::Latest)
            .with_vsync(false)
            //.with_double_buffer(Some(false))
            .build_windowed(window, &events_loop)
            .unwrap()
            .split()
    };
    let context = unsafe { context.make_current().unwrap() };

    let mut size = window.get_inner_size().unwrap().into();
    let dpi = window.get_hidpi_factor();

    let mut renderer = renderer::Renderer::new(&events_loop, context, dpi, size);
    let mut constraint = Constraint {
        min_width: 0.0,
        min_height: 0.0,
        max_width: size.width,
        max_height: size.height,
    };
    element.layout(&state, &ctx, constraint);

    let mut mouse_pos = LogicalPosition::new(0.0, 0.0);

    events_loop.run_forever(|e| {
        ctx.dirty = false;
        trace!("event: {:#?}", e);
        match dbg!(e) {
            GlutinEvent::WindowEvent {
                window_id: _,
                event,
            } => match event {
                WindowEvent::Resized(s) => {
                    //for now
                    ctx.dirty = true;
                    size = s.into();
                    renderer.resize(size);
                    constraint.max_height = size.height;
                    constraint.max_width = size.width;
                }
                WindowEvent::Focused(true) => {
                    ctx.dirty = true;
                }
                WindowEvent::CloseRequested => {
                    return ControlFlow::Break;
                }
                WindowEvent::CursorMoved {
                    device_id: _,
                    position,
                    modifiers: _,
                } => mouse_pos = position,
                WindowEvent::MouseInput {
                    device_id: _,
                    state: s,
                    button: _,
                    modifiers: _,
                } => {
                    let ty = match s {
                        ElementState::Pressed => PointerEvent::Pressed,
                        ElementState::Released => PointerEvent::Released,
                    };
                    let event = Event::Pointer {
                        position: Position::new(mouse_pos.x as f32, mouse_pos.y as f32),
                        event: ty,
                    };
                    element.event(&mut state, &mut ctx, event);
                }
                _ => {}
            },
            _ => {}
        }
        if ctx.dirty {
            {
                let mut builder = RenderBuilder::new(&mut renderer);
                element.layout(&state, &ctx, constraint);
                element.render(&ctx, &mut builder, Position::zero());
            }
            renderer.frame();
            renderer.frame();
        }
        ControlFlow::Continue
    });
}
