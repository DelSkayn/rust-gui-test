use crate::render::Renderer;
use crate::ty::*;
use webrender::{
    api::{
        BorderRadius, BoxShadowClipMode, ColorF, DisplayListBuilder, LayoutPixel,
        LayoutPrimitiveInfo, SpaceAndClipInfo, Transaction,
    },
    euclid::{TypedPoint2D, TypedRect, TypedSize2D},
};

impl From<Color> for ColorF {
    fn from(c: Color) -> Self {
        ColorF::new(c.r, c.g, c.b, c.a)
    }
}

impl From<Size> for TypedSize2D<f32, LayoutPixel> {
    fn from(s: Size) -> Self {
        TypedSize2D::new(s.width as f32, s.height as f32)
    }
}

impl From<Position> for TypedPoint2D<f32, LayoutPixel> {
    fn from(p: Position) -> Self {
        TypedPoint2D::new(p.x as f32, p.y as f32)
    }
}

impl From<Rect> for TypedRect<f32, LayoutPixel> {
    fn from(r: Rect) -> Self {
        TypedRect::new(r.position.into(), r.size.into())
    }
}

pub struct RenderBuilder<'a> {
    renderer: &'a mut Renderer,
    builder: Option<DisplayListBuilder>,
}

impl<'a> RenderBuilder<'a> {
    pub fn new(render: &'a mut Renderer) -> Self {
        let builder = DisplayListBuilder::new(render.pipeline_id, render.size.into());
        RenderBuilder {
            renderer: render,
            builder: Some(builder),
        }
    }

    pub fn rect(&mut self, rect: Rect, color: Color) {
        let info = LayoutPrimitiveInfo::new(rect.into());
        let color = color.into();
        let space_clip = SpaceAndClipInfo::root_scroll(self.renderer.pipeline_id);
        self.builder
            .as_mut()
            .unwrap()
            .push_rect(&info, &space_clip, color);
    }

    pub fn shadow(
        &mut self,
        rect: Rect,
        bounds: Rect,
        offset: (f32, f32),
        color: Color,
        blur: f32,
    ) {
        let info = LayoutPrimitiveInfo::new(rect.into());
        let space_clip = SpaceAndClipInfo::root_scroll(self.renderer.pipeline_id);
        self.builder.as_mut().unwrap().push_box_shadow(
            &info,
            &space_clip,
            bounds.into(),
            offset.into(),
            color.into(),
            blur,
            1.0,
            BorderRadius::zero(),
            BoxShadowClipMode::Outset,
        );
    }
}

impl<'a> Drop for RenderBuilder<'a> {
    fn drop(&mut self) {
        let mut txn = Transaction::new();
        let builder = self.builder.take().unwrap();
        let size = builder.content_size();
        txn.set_display_list(
            self.renderer.epoch,
            Some(ColorF::new(1.0, 1.0, 1.0, 1.0)),
            size,
            builder.finalize(),
            true,
        );
        txn.set_root_pipeline(self.renderer.pipeline_id);
        txn.generate_frame();
        self.renderer
            .api
            .send_transaction(self.renderer.document_id, txn);
    }
}
