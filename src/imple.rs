//! Implementations of traits and related structs.
use crate::{ty::*, Ctx, Element, Event, EventHit, RenderBuilder};

impl<S> Element<S> for Box<dyn Element<S>> {
    fn layout(&mut self, state: &S, ctx: &Ctx, constraint: Constraint) -> Size {
        (**self).layout(state, ctx, constraint)
    }

    fn flex(&self, s: &S) -> Flex {
        (**self).flex(s)
    }

    fn event(&mut self, state: &mut S, ctx: &mut Ctx, event: Event) -> EventHit {
        (**self).event(state, ctx, event)
    }

    fn render<'a>(&self, ctx: &Ctx, builder: &mut RenderBuilder<'a>, position: Position) {
        (**self).render(ctx, builder, position)
    }
}
