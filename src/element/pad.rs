use crate::{
    ty::{self, Constraint, Flex, Position, Size},
    Bind, Ctx, Element, Event, EventHit, RenderBuilder,
};
use std::fmt::Debug;
use std::marker::PhantomData;

/// Applies padding to its child element.
#[derive(Derivative)]
#[derivative(Debug(bound = "C: Debug"))]
pub struct Pad<S, C: Element<S>, P: Bind<S, ty::Padding>> {
    pub child: C,
    #[derivative(Debug = "ignore")]
    pub padding: P,
    pub padding_layout: ty::Padding,
    size: Size,
    #[derivative(Debug = "ignore")]
    __marker: PhantomData<S>,
}

impl<S, C: Element<S>, P: Bind<S, ty::Padding>> Pad<S, C, P> {
    pub fn new(padding: P, child: C) -> Self {
        Pad {
            child,
            padding,
            padding_layout: ty::Padding::all(0.0),
            size: Size::zero(),
            __marker: PhantomData,
        }
    }
}

impl<S, C: Element<S>, P: Bind<S, ty::Padding>> Element<S> for Pad<S, C, P> {
    fn layout(&mut self, state: &S, ctx: &Ctx, constraint: Constraint) -> Size {
        self.padding_layout = *self.padding.bind(state);
        let constraint = self.padding_layout.constrain(constraint);

        self.size = self.child.layout(state, ctx, constraint);
        self.size.width += self.padding_layout.horizontal();
        self.size.height += self.padding_layout.vertical();
        self.size
    }

    fn event(&mut self, state: &mut S, ctx: &mut Ctx, event: Event) -> EventHit {
        let event = event.adjust_position(Position {
            x: self.padding_layout.left,
            y: self.padding_layout.top,
        });
        self.child.event(state, ctx, event)
    }

    fn render<'a>(&self, ctx: &Ctx, builder: &mut RenderBuilder<'a>, mut position: Position) {
        position.x += self.padding_layout.left;
        position.y += self.padding_layout.top;
        self.child.render(ctx, builder, position);
    }

    fn flex(&self, state: &S) -> Flex {
        self.child.flex(state)
    }
}
