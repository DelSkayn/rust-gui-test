use crate::{
    ty::{Color, Rect},
    Constraint, Ctx, Element, Event, EventHit, Position, RenderBuilder, Size,
};

/// Draws a colored rectangle. Cannot have children.
#[derive(Debug)]
pub struct Fill {
    pub color: Color,
    size: Size,
}

impl Default for Fill {
    fn default() -> Self {
        Fill {
            color: Color::new(0.0, 0.0, 0.0, 1.0),
            size: Size::zero(),
        }
    }
}

impl Fill {
    pub fn new(color: Color) -> Self {
        Fill {
            color,
            size: Size::zero(),
        }
    }
}

impl<S> Element<S> for Fill {
    fn layout(&mut self, _: &S, _: &Ctx, constraint: Constraint) -> Size {
        assert!(constraint.is_bounded());
        self.size = constraint.biggest();
        self.size
    }

    fn event(&mut self, _: &mut S, _: &mut Ctx, _: Event) -> EventHit {
        EventHit::Passed
    }

    fn render<'a>(&self, _: &Ctx, render: &mut RenderBuilder<'a>, position: Position) {
        let rect = Rect {
            position,
            size: self.size,
        };
        render.rect(rect, self.color);
        let mut bound = rect;
        bound.size += Size::new(4.0, 4.0);
        //render.shadow(rect, bound, (4.0, 4.0), Color::default(), 4.0);

        dbg!(rect);
    }
}
