use crate::{ty::*, Element, EventHit,RenderBuilder,element::ElementCtx,event::{Event,PointerEvent},Ctx};
use std::marker::PhantomData;

pub struct Hit<S, C: Element<S>, F: Fn(&mut S)> {
    on_hit: F,
    child: ElementCtx<S,C>,
    size: Size,
    __marker: PhantomData<S>,
}

impl<S, C: Element<S>, F: Fn(&mut S)> Hit<S, C, F> {
    pub fn new(f: F, c: C) -> Self {
        Hit {
            on_hit: f,
            child: ElementCtx::new(c),
            size: Size::zero(),
            __marker: PhantomData,
        }
    }
}

impl<S, C: Element<S>, F: Fn(&mut S)> Element<S> for Hit<S, C, F> {
    fn layout(&mut self, state: &S,_: &Ctx, constraint: Constraint) -> Size {
        self.size = self.child.layout(state, constraint);
        self.size
    }

    fn event(&mut self, state: &mut S, ctx: &mut Ctx, event: Event) -> EventHit{
        match event{
            Event::Pointer{ position, event} => {
                match event{
                    PointerEvent::Pressed => {
                        if position.within_size(self.size) {
                            ctx.set_active();
                            ctx.set_focused();
                        }
                        return EventHit::Taken;
                    },
                    PointerEvent::Released => {
                        ctx.unset_active();
                        ctx.make_dirty();
                        (self.on_hit)(state);
                        return EventHit::Taken;
                    }
                    _ => {},
                }
            }
        }

        match self.child.event(state,ctx,event){
            EventHit::Taken => EventHit::Taken,
            _ => EventHit::Passed,
        }
    }

    fn render<'a>(&self,_: &Ctx, builder: &mut RenderBuilder<'a>, position: Position){
        self.child.render(builder, position)
    }

    fn flex(&self, state: &S) -> Flex {
        self.child.flex(state)
    }
}
