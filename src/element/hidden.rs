use crate::{ty::*, Bind, Element, EventHit, RenderBuilder,Ctx,Event};
use std::{fmt::Debug, marker::PhantomData};

/// Allows child elements to be hidden depending on state.
#[derive(Derivative)]
#[derivative(Debug(bound = "C: Debug,H: Debug"))]
pub struct Hidden<S, C: Element<S>, H: Bind<S, bool>> {
    pub hidden: H,
    pub child: C,
    pub is_hidden: bool,
    __marker: PhantomData<S>,
}

impl<S, C: Element<S>, H: Bind<S, bool>> Hidden<S, C, H> {
    pub fn new(hidden: H, child: C) -> Self {
        Hidden {
            hidden,
            child,
            is_hidden: false,
            __marker: PhantomData,
        }
    }
}

impl<S, C: Element<S>, H: Bind<S, bool>> Element<S> for Hidden<S, C, H> {
    fn layout(&mut self, state: &S,ctx: &Ctx, constraint: Constraint) -> Size {
        self.is_hidden = *self.hidden.bind(state);
        if !self.is_hidden {
            self.child.layout(state,ctx, constraint)
        } else {
            Size::zero()
        }
    }

    fn flex(&self, state: &S) -> Flex {
        if !self.hidden.bind(state) {
            self.child.flex(state)
        } else {
            Flex::none()
        }
    }

    fn event(&mut self, state: &mut S, ctx: &mut Ctx, event: Event) -> EventHit{
        if !self.is_hidden {
            self.child.event(state, ctx, event)
        }else{
            EventHit::Passed
        }
    }

    fn render<'a>(&self,ctx: &Ctx, builder: &mut RenderBuilder<'a>, position: Position) {
        if !self.is_hidden {
            self.child.render(ctx,builder, position)
        }
    }
}
