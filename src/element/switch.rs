use std::marker::PhantomData;
use crate::ty::*;
use crate::{Element,Layout};

pub enum State{
    Foo(f32),
    Bar(f32),
}


pub trait DynStateElement{
    fn dyn_layout(&self, constraint: Constraint) -> (Size,Box<dyn Layout>);

    fn dyn_flex(&self) -> Flex;
}

impl<'a,S,E: Element<S>> DynStateElement for (&'a S,&'a E){
    fn dyn_layout(&self,constraint: Constraint) -> (Size,Box<dyn Layout>){
        self.1.layout(self.0)
    }

    fn dyn_flex(&self) -> Flex{
        self.flex()
    }
}

pub trait Select<S>{
    fn select(&self,s: &S) -> &dyn DynStateElement;
}

impl<S> Select<S> for Fn(S) -> &dyn DynStateElement{
    fn select(&self,s: &S) -> &dyn DynStateElement{
        self(s)
    }
}

/*
pub struct Switch<S,C: Select<S>>{
    binding: B,
    __marker: PhantomData<S>,
}
*/
