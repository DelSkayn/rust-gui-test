use crate::{
    ty::{Constraint, Flex, Position, Size},
    Bind, Ctx, Element, Event, EventHit, RenderBuilder,
};
use std::fmt::Debug;
use std::marker::PhantomData;

/// Allows one to specify a [Flex](crate::ty::Flex) value for itself and its childeren.
#[derive(Derivative)]
#[derivative(Clone(bound = "C: Clone,F: Clone"), Debug(bound = "C: Debug,F: Debug"))]
pub struct Flexible<S, C: Element<S>, F: Bind<S, Flex>> {
    pub child: C,
    pub flex: F,
    #[derivative(Debug = "ignore")]
    __marker: PhantomData<S>,
}

impl<S, C: Element<S>, F: Bind<S, Flex>> Flexible<S, C, F> {
    pub fn new(flex: F, child: C) -> Self {
        Flexible {
            child,
            flex,
            __marker: PhantomData,
        }
    }
}

impl<S, C: Element<S>, F: Bind<S, Flex>> Element<S> for Flexible<S, C, F> {
    fn layout(&mut self, state: &S, ctx: &Ctx, constraint: Constraint) -> Size {
        self.child.layout(state, ctx, constraint)
    }

    fn event(&mut self, state: &mut S, ctx: &mut Ctx, event: Event) -> EventHit {
        self.child.event(state, ctx, event)
    }

    fn render<'a>(&self, ctx: &Ctx, builder: &mut RenderBuilder<'a>, position: Position) {
        self.child.render(ctx, builder, position)
    }

    fn flex(&self, state: &S) -> Flex {
        *self.flex.bind(state)
    }
}
