use crate::{
    ty::{Color, Rect},
    Constraint, Ctx, Element, Event, EventHit, Position, RenderBuilder, Size,
};
use std::marker::PhantomData;

/// Renders a rectangle, can have childeren.
#[derive(Debug)]
pub struct Panel<S, E: Element<S>> {
    pub color: Color,
    size: Size,
    child: E,
    __marker: PhantomData<S>,
}

impl<S, E: Element<S>> Panel<S, E> {
    pub fn new(color: Color, child: E) -> Self {
        Panel {
            color,
            size: Size::zero(),
            child,
            __marker: PhantomData,
        }
    }
}

impl<S, E: Element<S>> Element<S> for Panel<S, E> {
    fn layout(&mut self, state: &S, ctx: &Ctx, constraint: Constraint) -> Size {
        assert!(constraint.is_bounded());
        self.child.layout(state, ctx, constraint);
        self.size = constraint.biggest();
        self.size
    }

    fn event(&mut self, state: &mut S, ctx: &mut Ctx, event: Event) -> EventHit {
        self.child.event(state, ctx, event)
    }

    fn render<'a>(&self, ctx: &Ctx, builder: &mut RenderBuilder<'a>, position: Position) {
        let rect = Rect {
            position,
            size: self.size,
        };
        builder.rect(rect, self.color);
        let mut bound = rect;
        bound.size += Size::new(4.0, 4.0);
        builder.shadow(rect, bound, (4.0, 4.0), Color::default(), 4.0);
        self.child.render(ctx, builder, position);
    }
}
