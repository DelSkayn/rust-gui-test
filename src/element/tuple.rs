use crate::{ty::*, Bind, Element,  EventHit, RenderBuilder, Value,element::ElementCtx,Event,Ctx};
use std::f32;
use std::marker::PhantomData;


struct TupleChild<S>{
    child: ElementCtx<S,Box<dyn Element<S>>>,
    position: Position,
}

/// Arranges a static amount of childeren an a certain axis.
pub struct Tuple<S, A: Bind<S, Axis>> {
    children: Vec<TupleChild<S>>,
    pub axis: A,
    stored_axis: Axis,
    __marker: PhantomData<S>,
}

impl<S, A: Bind<S, Axis>> Tuple<S,A> {
    pub fn new(axis: A) -> Self {
        Tuple {
            children: Vec::new(),
            axis,
            stored_axis: Axis::Horizontal,
            __marker: PhantomData,
        }
    }

    pub fn add_child<C: Element<S> + 'static>(mut self,child: C) -> Self{
        self.children.push(TupleChild{
            child: ElementCtx::new(Box::new(child) as Box<dyn Element<S>>),
            position: Position::zero(),
        });
        self
    }
}

impl<S> Tuple<S, Value<Axis>> {
    pub fn row() -> Self {
        Tuple {
            axis: Value(Axis::Vertical),
            stored_axis: Axis::Vertical,
            children: Vec::new(),
            __marker: PhantomData,
        }
    }

    pub fn column() -> Self {
        Tuple {
            axis: Value(Axis::Horizontal),
            stored_axis: Axis::Horizontal,
            children: Vec::new(),
            __marker: PhantomData,
        }
    }
}

impl<S, A: Bind<S, Axis>> Element<S> for Tuple<S, A> {

    fn layout(&mut self, state: &S, _: &Ctx, constraint: Constraint) -> Size {
        self.stored_axis = *self.axis.bind(state);
        // THe constraint for elements without flex
        let child_constraint = match self.stored_axis {
            Axis::Horizontal => Constraint {
                max_width: f32::INFINITY,
                ..constraint
            },
            Axis::Vertical => Constraint {
                max_height: f32::INFINITY,
                ..constraint
            },
        };

        let mut used_size = 0.0;
        let child_iter = self.children.iter_mut();
        let mut total_flex = 0.0;

        // Layout non flex element and calc amount of height used and the total flex for all
        // elements
        for element in child_iter {
            let flex = element.child.flex(state);
            if flex == Flex::none() {
                let size = element.child.layout(state, child_constraint);
                match self.stored_axis {
                    Axis::Horizontal => used_size += size.height,
                    Axis::Vertical => used_size += size.height,
                }
            } else {
                total_flex += flex.0;
            }
        }

        let free_size = match self.stored_axis {
            Axis::Horizontal => constraint.max_width - used_size,
            Axis::Vertical => constraint.max_height - used_size,
        };

        // Layout flex elements in the remaining height left.
        let child_iter = self.children.iter_mut();
        for element in child_iter {
            let flex = element.child.flex(state);
            if flex != Flex::none() {
                let allowed_size = free_size * (flex.0 / total_flex);
                let constraint = match self.stored_axis {
                    Axis::Horizontal => Constraint {
                        max_width: allowed_size,
                        ..constraint
                    },
                    Axis::Vertical => Constraint {
                        max_height: allowed_size,
                        ..constraint
                    },
                };
                dbg!(constraint);
                let size = element.child.layout(state, constraint);
                match self.stored_axis {
                    Axis::Horizontal => {
                        element.position = Position {
                            x: used_size,
                            y: 0.0,
                        };
                        used_size += size.width;
                    }
                    Axis::Vertical => {
                        element.position = Position {
                            x: 0.0,
                            y: used_size,
                        };
                        used_size += size.height;
                    }
                }
            }
        }
        let mut size = constraint.biggest();
        match self.stored_axis {
            Axis::Horizontal => {
                size.width = used_size;
            }
            Axis::Vertical => {
                size.height = used_size;
            }
        }
        size
    }

    fn event(&mut self, state: &mut S, ctx: &mut Ctx,event: Event) -> EventHit{
        let child_iter = self.children.iter_mut();
        for element in child_iter {
            let event = event.adjust_position(element.position);
            match element.child.event(state,ctx,event){
                EventHit::Taken => return EventHit::Taken,
                _ => {},
            }
        }
        EventHit::Passed
    }

    fn render<'a>(&self,_: &Ctx, builder: &mut RenderBuilder<'a>, base_position: Position) {
        for element in self.children.iter(){
            let position = base_position + element.position;
            element.child.render(builder, position);
        }
    }
}
