//! Pre implemented elements

mod tuple;
pub use tuple::Tuple;
mod pad;
pub use pad::Pad;
mod flexible;
pub use flexible::Flexible;
mod fill;
pub use fill::Fill;
mod panel;
pub use panel::Panel;
mod hidden;
pub use hidden::Hidden;
mod hit;
pub use hit::Hit;

use crate::{
    ty::{self, Constraint, Position, Size},
    Element, Event, EventHit, RenderBuilder,
};
use std::{fmt::Debug, marker::PhantomData};

#[derive(Debug, Clone, Default)]
pub struct Ctx {
    pub is_active: bool,
    pub has_active: bool,
    pub is_focused: bool,
    pub has_focused: bool,

    pub dirty: bool,
}

impl Ctx {
    fn make_dirty(&mut self) {
        self.dirty = true;
    }

    fn set_focused(&mut self) {
        self.has_focused = true;
        self.is_focused = true;
    }

    fn set_active(&mut self) {
        self.has_active = true;
        self.is_active = true;
    }

    fn unset_active(&mut self) {
        self.has_focused = false;
        self.is_focused = false;
    }
}

#[derive(Derivative, Clone)]
#[derivative(Debug(bound = "E: Debug"))]
pub struct ElementCtx<S, E> {
    pub context: Ctx,
    pub element: E,
    __marker: PhantomData<S>,
}

impl<S, E: Element<S>> ElementCtx<S, E> {
    pub fn new(element: E) -> Self {
        ElementCtx {
            element,
            context: Ctx::default(),
            __marker: PhantomData,
        }
    }

    pub fn layout(&mut self, state: &S, constraint: Constraint) -> Size {
        self.element.layout(state, &self.context, constraint)
    }

    pub fn event(&mut self, state: &mut S, ctx: &mut Ctx, event: Event) -> EventHit {
        self.context.has_focused = false;
        self.context.has_active = false;

        let res = self.element.event(state, &mut self.context, event);

        self.context.has_focused |= self.context.is_focused;

        ctx.has_focused |= self.context.has_focused;
        ctx.has_active |= self.context.has_active;
        ctx.dirty = self.context.dirty;
        res
    }

    pub fn render<'a>(&self, render: &mut RenderBuilder<'a>, position: Position) {
        self.element.render(&self.context, render, position)
    }

    pub fn flex(&self, state: &S) -> ty::Flex {
        self.element.flex(state)
    }
}
