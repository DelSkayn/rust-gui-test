//!
//! # Example
//! ```
//! fn main() {
//!     let element = element::Padding::new(
//!         Value(Padding::all(10.0)),
//!         element::Tuple::row((
//!             element::Hidden::new(
//!                 closure(|s| s),
//!                 element::Flex::new(
//!                     Value(Flex::flex(1.0)),
//!                     element::Padding::new(
//!                         Value(Padding::all(5.0)),
//!                         element::Hit::new(|s| *s = true, element::Fill::new(Color::red())),
//!                     ),
//!                 ),
//!             ),
//!             element::Flex::new(
//!                 Value(Flex::flex(1.0)),
//!                 element::Padding::new(
//!                     Value(Padding::all(5.0)),
//!                     element::Hit::new(|s| *s = false, element::Fill::new(Color::red())),
//!                 ),
//!             ),
//!         )),
//!     );
//!     render::render(false, element);
//!     info!("quiting");
//! }
//! ```
//!
//!

#[macro_use]
extern crate derivative;
#[macro_use]
extern crate log;

pub mod ty;
pub use ty::*;
mod bind;
pub use bind::*;
pub mod element;
use element::*;
pub mod imple;
pub mod render;
pub use element::Ctx;
use render::RenderBuilder;
pub mod event;
pub use event::Event;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum EventHit {
    /// The is not yet handled
    Passed,
    /// The is already handled by a child element
    Taken,
}

/// The core trait of the library.
///
/// An element is a small component of a widget which has some way of composing it self and its
/// childeren.
/// TODO: Phrase this better
pub trait Element<S> {
    fn layout(&mut self, state: &S, ctx: &Ctx, constraint: Constraint) -> Size;

    fn event(&mut self, state: &mut S, ctx: &mut Ctx, event: Event) -> EventHit;

    fn render<'a>(&self, ctx: &Ctx, render: &mut RenderBuilder<'a>, position: Position);

    //TODO: Extract from element trait, might be require somewhat more design as extracting it into
    //its own traid does not work.
    fn flex(&self, _: &S) -> Flex {
        Flex::none()
    }
}

fn main() {
    let childeren = Tuple::column()
        .add_child(Flexible::new(
            Value(Flex::flex(1.0)),
            Pad::new(Value(Padding::all(10.0)), Fill::new(Color::red())),
        ))
        .add_child(Flexible::new(
            Value(Flex::flex(1.0)),
            Pad::new(Value(Padding::all(10.0)), Fill::new(Color::red())),
        ));

    let element = Pad::new(
        Value(Padding::all(10.0)),
        Tuple::row()
            .add_child(Hidden::new(
                closure(|s| s),
                Flexible::new(
                    Value(Flex::flex(1.0)),
                    Pad::new(
                        Value(Padding::all(5.0)),
                        Hit::new(|s| *s = true, Fill::new(Color::red())),
                    ),
                ),
            ))
            .add_child(Flexible::new(
                Value(Flex::flex(1.0)),
                Pad::new(
                    Value(Padding::all(5.0)),
                    Hit::new(|s| *s = false, Fill::new(Color::red())),
                ),
            ))
            .add_child(Flexible::new(
                Value(Flex::flex(1.0)),
                Pad::new(
                    Value(Padding::all(5.0)),
                    Panel::new(Color::blue(), childeren),
                ),
            )),
    );
    render::render(false, element);
    info!("quiting");
}
