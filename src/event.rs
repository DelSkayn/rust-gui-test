use crate::ty::Position;
//use winit::

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum PointerEvent {
    Moved,
    Pressed,
    Released,
}

#[derive(Debug, Clone, Copy)]
pub enum Event {
    Pointer {
        position: Position,
        event: PointerEvent,
    }, //Key{
}

impl Event {
    pub fn adjust_position(self, adjust: Position) -> Self {
        match self {
            Event::Pointer { position, event } => Event::Pointer {
                position: position - adjust,
                event,
            },
            //x => x
        }
    }
}
