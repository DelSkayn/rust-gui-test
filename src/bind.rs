use std::fmt::Debug;

/// Used for binding state to variables used in elements.
///
/// Implemented for the [Value](Value) struct and for closures of the form `Fn(&A) -> &B`.
/// Altough the closures often require the [closure](closure) funtions to resolve lifetimes correctly.
pub trait Bind<A, B> {
    fn bind<'a, 'b: 'a>(&'b self, _: &'a A) -> &'a B;
}

pub trait BindMut<A, B> {
    fn bind_mut<'a, 'b: 'a>(&'b mut self, _: &'a mut A) -> &'a mut B;
}

impl<A, B, Fr> Bind<A, B> for Fr
where
    Fr: for<'r> Fn(&'r A) -> &'r B,
{
    fn bind<'a, 'b: 'a>(&'b self, a: &'a A) -> &'a B {
        self(a)
    }
}

impl<A, B, Fr, Fm> Bind<A, B> for (Fr, Fm)
where
    Fr: for<'r> Fn(&'r A) -> &'r B,
    Fm: for<'r> FnMut(&'r mut A) -> &'r mut B,
{
    fn bind<'a, 'b: 'a>(&'b self, a: &'a A) -> &'a B {
        self.0(a)
    }
}

impl<A, B, Fr, Fm> BindMut<A, B> for (Fr, Fm)
where
    Fr: for<'r> Fn(&'r A) -> &'r B,
    Fm: for<'r> FnMut(&'r mut A) -> &'r mut B,
{
    fn bind_mut<'a, 'b: 'a>(&'b mut self, a: &'a mut A) -> &'a mut B {
        self.1(a)
    }
}

/// A wrapping struct which allows a owned value to be used for a binding.
#[derive(Derivative)]
#[derivative(
    Debug(bound = "T: Debug"),
    Clone(bound = "T: Clone"),
    Copy(bound = "T: Copy")
)]
pub struct Value<T>(pub T);

impl<A, B> Bind<A, B> for Value<B> {
    fn bind<'a, 'b: 'a>(&'b self, _: &'a A) -> &'a B {
        &self.0
    }
}

/// Identity function which fixes wierd issues with resolving lifetimes on closures.
///
/// Use when passing a the constant closure as binding when the compiler returns an error about bound lifetimes.
/// Does nothing other then immediatly returning the argument
/// ```
/// let element = element::Hide::<bool,_,_>::new(
///     // The compile fails to infer the proper lifetime for naked closures
///     // |s| s
///
///     // When wrapping with the closure function the lifetimes are properly resolved.
///     closure(|s| s),
///     element::Pad::new(
///         Padding::all(50.0),
///         element::Fill,
///     )
/// );
///     
/// ```
pub fn closure<A, B, F>(x: F) -> F
where
    F: for<'a> Fn(&'a A) -> &'a B,
{
    x
}

/// Identity function which fixes wierd issues with resolving lifetimes on closures.
///
/// Use when passing a the mutable closure as binding when the compiler returns an error about bound lifetimes.
/// Does nothing other then immediatly returning the argument
/// ```
/// let element = element::Hide::<bool,_,_>::new(
///     // The compile fails to infer the proper lifetime for naked closures
///     // |s| s,
///
///     // When wrapping with the closure function the lifetimes are properly resolved.
///     closure(|s| s),
///     element::Pad::new(
///         Padding::all(50.0),
///         element::Fill,
///     )
/// );
///     
/// ```
pub fn closure_mut<A, B, F>(x: F) -> F
where
    F: for<'a> FnMut(&'a mut A) -> &'a mut B,
{
    x
}
