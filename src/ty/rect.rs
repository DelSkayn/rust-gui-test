pub use crate::ty::{Position, Size};

/// Position and size of rectangle.
#[derive(Debug, Copy, Clone, Default)]
pub struct Rect {
    pub position: Position,
    pub size: Size,
}

impl Rect {
    pub fn new(position: Position, size: Size) -> Self {
        Rect { position, size }
    }
}
