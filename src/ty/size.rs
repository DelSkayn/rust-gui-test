use std::ops;
use glutin::dpi::LogicalSize;

#[derive(Debug,Default,PartialEq,Clone,Copy)]
/// Represents a size of an object on the screen in logical units.
pub struct Size{
    /// The size in logical size of in the horizontal axis
    pub width: f32,
    /// The size in logical size of in the vertical axis
    pub height: f32,
}

impl Size{

    pub fn new(width: f32,height: f32) -> Self{
        Size{
            width,
            height,
        }
    }
    /// Returns wether the other size is contained within the current.
    /// ```
    /// let a = Size{
    ///     width: 2.0,
    ///     height: 2.0,
    /// };
    /// let b = Size{
    ///     width: 1.0,
    ///     height: 1.0,
    /// };
    /// assert!(a.contained(b));
    /// ```
    pub fn contained(&self,size: &Size) -> bool{
        self.width >= size.width && self.height >= size.height
    }

    /// Returns an object with both fields width and height zero.
    /// ```
    /// let a = Size::zero()
    /// assert!(a.width == 0.0 && a.height == 0.0)
    /// ```
    pub fn zero() -> Size{
        Size{
            width: 0.0,
            height: 0.0,
        }
    }
}

impl ops::Add for Size{
    type Output = Self;

    fn add(self, other: Size) -> Size{
        Size{
            width: self.width + other.width,
            height: self.height + other.height,
        }
    }
}

impl ops::AddAssign for Size{
    fn add_assign(&mut self,other: Self) {
        *self = Self{
            width: self.width + other.width,
            height: self.height + other.height,
        }
    }
}

impl ops::Sub for Size{
    type Output = Self;

    fn sub(self, other: Size) -> Size{
        Size{
            width: self.width - other.width,
            height: self.height - other.height,
        }
    }
}

impl ops::SubAssign for Size{
    fn sub_assign(&mut self,other: Self) {
        *self = Self{
            width: self.width - other.width,
            height: self.height - other.height,
        }
    }
}

impl ops::Neg for Size{
    type Output = Size ;

    fn neg(self) -> Self::Output{
        Size{
            width: -self.width,
            height: -self.height
        }
    }
}

impl From<LogicalSize> for Size{
    fn from(s: LogicalSize) -> Size{
        Size::new(s.width as f32,s.height as f32)
    }
}

impl From<Size> for LogicalSize{
    fn from(s: Size) -> LogicalSize{
        LogicalSize::new(s.width as f64,s.height as f64)
    }
}
