use crate::Size;
use std::f32;

/// This library uses a layout algorithm inspired by the flutter layout model.
///
/// layout constraints for layout.
/// 
/// A [Size] respects a BoxConstraints if, and only if, all of the following relations hold:
///
///  * min_width <= [Size]::width <= max_width
///  * min_height <= [Size]::height <= max_height
/// 
/// The constraints themselves must satisfy these relations:
/// 
///  * 0.0 <= min_width <= max_width <= [f32](std::f32)::[INFINITY](std::f32::INFINITY)
///  * 0.0 <= min_height <= max_height <= [f32](std::f32)::[INFINITY](std::f32::INFINITY)
/// 
/// [f32](std::f32)::[INFINITY](std::f32::INFINITY) is a legal value for each constraint.
/// 
/// # The box layout model
/// 
/// Render objects in the Flutter framework are laid out by a one-pass layout model which walks down the render tree passing constraints, then walks back up the render tree passing concrete geometry.  
/// For boxes, the constraints are BoxConstraints, which, as described herein, consist of four numbers: a minimum width minWidth, a maximum width maxWidth, a minimum height minHeight, and a maximum height maxHeight.  
/// The geometry for boxes consists of a Size, which must satisfy the constraints described above.  
/// Each RenderBox (the objects that provide the layout models for box widgets) receives BoxConstraints from its parent, then lays out each of its children, then picks a Size that satisfies the BoxConstraints.  
/// Render objects position their children independently of laying them out. Frequently, the parent will use the children's sizes to determine their position. A child does not know its position and will not necessarily be laid out again, or repainted, if its position changes.
///
/// [Size]: crate::Size
#[derive(Debug,PartialEq,Clone,Copy)]
pub struct Constraint {
    /// The minimum width that satisfies the constraint.
    pub min_width: f32,
    /// The maximum width that satisfies the constraint.
    pub max_width: f32,
    /// The minimum height that satisfies the constraint.
    pub min_height: f32,
    /// The maximum height that satisfies the constraint.
    pub max_height: f32,
}

impl Default for Constraint{
    fn default() -> Self{
        Constraint{
            min_width: 0.0,
            min_height: 0.0,
            max_width: f32::INFINITY,
            max_height: f32::INFINITY,
        }
    }
}

impl Constraint{
    /// Returns the biggest size that satisfies the constraint.
    /// # Panic
    /// This function panics if the constraint is not bounded.
    /// ```
    /// let constraint = Constraint{
    ///     max_width: 600.0,
    ///     max_height: 400.0,
    ///     ..Default::default(),
    /// }
    /// let size = constraint.biggest();
    /// assert_eq!(size.width,600.0);
    /// assert_eq!(size.height,400.0);
    /// ```
    pub fn biggest(&self) -> Size{
        assert!(self.is_bounded());
        Size{
            width: self.max_width,
            height: self.max_height,
        }
    }

    /// Returns the smallest size that satisfies the constraint.
    /// ```
    /// let constraint = Constraint{
    ///     min_width: 1.0,
    ///     min_height: 2.0,
    ///     ..Default::default(),
    /// }
    /// let size = constraint.smallest();
    /// assert_eq!(size.width,1.0);
    /// assert_eq!(size.height,2.0);
    /// ```
    pub fn smallest(&self) -> Size{
        Size{
            width: self.min_width,
            height: self.min_width
        }
    }

    pub fn within(&self,size: Size) -> bool{
        return self.min_width <= size.width && self.max_width >= size.width
            && self.min_height <= size.height && self.max_height >= size.height;
    }

    /// Returns wether all of the max values are not infinite.
    /// This would mean that a element that is valid in this constraint would be bounded in size.
    pub fn is_bounded(&self) -> bool{
        self.max_height != f32::INFINITY && self.max_width != f32::INFINITY
    }
}

