use crate::Constraint;

#[derive(Debug,Default,Clone,Copy)]
/// Type for containing the information for padding a container.
pub struct Padding{
    /// Padding on the left of the container
    pub left: f32,
    /// Padding on the right of the container
    pub right: f32,
    /// Padding on the top of the container
    pub top: f32,
    /// Padding on the bottom of the container
    pub bottom: f32,
}

impl Padding{
    /// Create a padding struct with all values the same
    pub fn all(a: f32) -> Self{
        Padding{
            left: a,
            right: a,
            top: a,
            bottom: a,
        }
    }

    /// Returns the amount of padding on the horizontal axis: left + right
    pub fn horizontal(&self) -> f32{
        self.left + self.right
    }

    /// Returns the amount fo padding on the vertical axis: top + bottom
    pub fn vertical(&self) -> f32{
        self.top + self.bottom
    }
    /// Apply padding to a constraint.
    /// Subtracts the width of the left an right padding from maxWidth
    /// and subtracts top and bottom from maxHeight
    pub fn constrain(&self,mut constraint: Constraint) -> Constraint{
        let width = self.left + self.right;
        let height = self.top + self.bottom;

        constraint.max_height = (constraint.max_height - height).max(constraint.min_height);
        constraint.max_width = (constraint.max_width - width).max(constraint.min_width);
        return constraint;
    }
}
