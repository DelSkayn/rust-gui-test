/// Represents color.
#[derive(Debug, Clone, Copy)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl Default for Color {
    fn default() -> Self {
        Color {
            r: 0.0,
            g: 0.0,
            b: 0.0,
            a: 1.0,
        }
    }
}

impl Color {
    pub fn new(r: f32, g: f32, b: f32, a: f32) -> Self {
        Color { r, g, b, a }
    }

    pub fn red() -> Self {
        Color {
            r: 1.0,
            ..Default::default()
        }
    }

    pub fn green() -> Self {
        Color {
            g: 1.0,
            ..Default::default()
        }
    }

    pub fn blue() -> Self {
        Color {
            b: 1.0,
            ..Default::default()
        }
    }

    pub fn black() -> Self {
        Color::default()
    }

    pub fn white() -> Self {
        Color {
            r: 1.0,
            b: 1.0,
            g: 1.0,
            a: 1.0,
        }
    }
}
