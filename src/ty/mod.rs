//! General pod data struct used throught the library

mod constraint;
pub use constraint::Constraint;
mod size;
pub use size::Size;
mod position;
pub use position::Position;
mod padding;
pub use padding::Padding;
mod color;
pub use color::Color;
mod rect;
pub use rect::Rect;

/// Value which represents the portion of a space a flexible element should recieve.
#[derive(Clone, Copy, PartialEq, PartialOrd, Debug, Default)]
pub struct Flex(pub f32);

impl Flex {
    /// Returns a Flex value which indicates no flex
    pub fn none() -> Self {
        Flex(0.0)
    }

    /// Create a flex value from f32.
    pub fn flex(v: f32) -> Self {
        Flex(v)
    }

    /// Tests wether a Flex value has no flex.
    pub fn is_none(&self) -> bool {
        self.0 == Flex::none().0
    }
}

impl From<f32> for Flex {
    fn from(f: f32) -> Flex {
        Flex(f)
    }
}

/// An axis used in elements often for layout.
#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum Axis {
    Vertical,
    Horizontal,
}
