use crate::ty::Size;
use std::ops;

#[derive(Debug, PartialEq, Clone, Copy,Default)]
/// A struct representing a position.
pub struct Position {
    /// Position in the horizontal axis
    pub x: f32,
    /// Position in the vertical axis
    pub y: f32,
}

impl Position {
    pub fn new(x: f32,y: f32) -> Position{
        Position{
            x,
            y,
        }
    }
    /// creates a position object with both fields x and y 0.0
    pub fn zero() -> Self {
        Position { x: 0.0, y: 0.0 }
    }

    pub fn within_size(&self, size: Size) -> bool {
        self.x <= size.width && self.y <= size.height
    }

    pub fn has_negative_values(&self) -> bool {
        self.x < 0.0 || self.y < 0.0
    }
}

impl ops::Add for Position {
    type Output = Self;

    fn add(self, other: Position) -> Position {
        Position {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::AddAssign for Position {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::Sub for Position {
    type Output = Self;

    fn sub(self, other: Position) -> Position {
        Position {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl ops::SubAssign for Position {
    fn sub_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl ops::Neg for Position {
    type Output = Position;

    fn neg(self) -> Self::Output {
        Position {
            x: -self.x,
            y: -self.y,
        }
    }
}
