#[derive(Debug, Clone)]
pub struct Shadow {
    pub blur_radius: f32,
    pub offset: (f32, f32, f32),
    pub color: Color,
}

pub enum StyleValue<'a, T> {
    Ref(&'a T),
    Value(T),
}

impl Shadow {}

#[derive(Debug, Clone)]
pub struct Style {
    pub background_color: Color,
    pub drop_shadow: Option<Shadow>,
}

impl Default for Style {
    fn default() -> Self {
        Style {
            background_color: Color::default(),
            drop_shadow: None,
        }
    }
}
